
# Implmentation of Passport Authorization using Node

This Repository displays the implmentation of Passport Authorization using Node. It uses mongodb client for the database.


## Authors

- [@darshan3104](https://gitlab.com/darshan3104)


## Installation

Install my-project with npm

```bash
  cd node-mongo-passport-auth
  npm install

```
    
## Lessons Learned

This project helped me to understand the steps to setup Passport Authentication. It will act as a boiler plate code or a reference for my further projects.

