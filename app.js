const express = require("express");
const debug = require("debug")("app");
const morgan = require("morgan");
const path = require("path");
const dotenv = require('dotenv');
dotenv.config();

const PORT = process.env.PORT || 3000;
const app = express();

// log the request details in terminal using morgan
app.use(morgan("tiny"));
// serve the static page
app.use(express.static(path.join(__dirname, "/public/")));

// view engine setup
app.set("views", "./src/views");
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  res.render("index", { title: "Welcome" });
});

app.get("/signin", (req, res) => {
  res.render("signin", { title: "Sign In" });
});

app.get("/signup", (req, res) => {
  res.render("signup", { title: "Sign Up" });
});

// listen to the server with specified port
app.listen(PORT, () => {
  debug(`Server is running on ${PORT}...`);
});
